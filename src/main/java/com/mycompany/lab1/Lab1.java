/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1;
import java.util.Scanner;

/**
 *
 * @author kanthapong
 */
public class Lab1 {
    private static final int SIZE = 3;
    private static char[][] board;
    private static char Player;

    public static void main(String[] args) {
      createBoard();
      Player = 'X';
      boolean game = false;
      
      while(!game){
          drawBoard();
           int[] move = getPlayerMove();
           Move(move[0], move[1]); 
           
           if (check_Win()) {
                game = true;
                drawBoard();
                System.out.println("Player " + Player + " wins!");
            } else if (BoardFull()) {
                game = true;
                drawBoard();
                System.out.println("It's a draw!");
            } else {
                Player = (Player == 'X') ? 'O' : 'X';
            }
      }
    }
    
    private static void createBoard() {
        board = new char[SIZE][SIZE];
        for (int row = 0; row < SIZE; row++) {
            for (int col = 0; col < SIZE; col++) {
                board[row][col] = '-';
            }
        }
    }
    
    private static void drawBoard() {
        System.out.println("-----------");
        for (int row = 0; row < SIZE; row++) {
            for (int col = 0; col < SIZE; col++) {
                System.out.print(board[row][col] + " | ");
            }
            System.out.println();
        }
        System.out.println("-----------");
    }
    
    private static int[] getPlayerMove() {
        Scanner scanner = new Scanner(System.in);
        int[] move = new int[2];

        System.out.print("Player " + Player + ", please input (row column): "); //รับค่าเข้าผ่านคีย์บอร์ดนับindex 0-2 ตามตาราง row/col
        move[0] = scanner.nextInt();
        move[1] = scanner.nextInt();

        return move;
    }
    
    private static void Move(int row, int col) {
        if (row < 0 || row >= SIZE || col < 0 || col >= SIZE || board[row][col] != '-') {
            System.out.println("Not available, please select again."); //กรณีตำแหน่งนั้นไม่ว่าง
            return;
        }

        board[row][col] = Player;
    }

    private static boolean check_Win() {
        // ตรวจสอบแนวนอน
        for (int row = 0; row < SIZE; row++) {
            if (board[row][0] != '-' && board[row][0] == board[row][1] && board[row][0] == board[row][2]) {
                return true;
            }
        }

        // ตรวจสอบแนวตั้ง
        for (int col = 0; col < SIZE; col++) {
            if (board[0][col] != '-' && board[0][col] == board[1][col] && board[0][col] == board[2][col]) {
                return true;
            }
        }

        // ตรวจสอบแนวทแยงมุม
        if (board[0][0] != '-' && board[0][0] == board[1][1] && board[0][0] == board[2][2]) {
            return true;
        }
        if  (board[0][2] != '-' && board[0][2] == board[1][1] && board[0][2] == board[2][0]) {
            return true;
        }

        return false;
    }
    
    private static boolean BoardFull() {
        for (int row = 0; row < SIZE; row++) {
            for (int col = 0; col < SIZE; col++) {
                if (board[row][col] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

}
